package script.miner;

import script.miner.data.ProMinerModel;
import script.miner.data.RockCluster;
import script.miner.task.Banking;
import script.miner.task.Mining;
import script.miner.task.Traverse;
import script.api.framework.PaintStatistic;
import script.api.framework.ProScript;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;

/**
 * The script 'main' class. This class is the first thing that gets executed whenever a script is ran.
 * Here the developer submits the tasks that are meant to be executed by the script.
 * Also, any other pre-configuration has to be done in this class.
 */
@ScriptMeta(name = "Pro-Miner",
        developer = "Burak",
        version = 0.32,
        desc = "Mines rocks at a specified location!")
public final class ProMiner extends ProScript<ProMinerModel> implements RenderListener {

    private int startExperience = 0;

    public ProMiner() {
        super(new ProMinerModel());
    }

    /**
     * This gets executed on script launch.
     */
    @Override
    public void onStart() {
        paint.submit("State", new PaintStatistic(() -> {
            Task task = getCurrent();
            return task == null ? "Determining..." : task.getClass().getSimpleName();
        }));

        paint.submit("Experience", new PaintStatistic(runtime, () -> {
            if (startExperience == 0 && Game.isLoggedIn()) {
                startExperience = Skills.getExperience(Skill.MINING);
            }
            return Skills.getExperience(Skill.MINING) - startExperience;
        }));

        paint.submit("Current level", new PaintStatistic(() -> Integer.toString(Skills.getLevel(Skill.MINING))));

        paint.submit("Mined", new PaintStatistic(runtime, () -> {
            double exp = Skills.getExperience(Skill.MINING) - startExperience;
            if (exp == 0) {
                return 0;
            }
            return (int) (exp / model.getSelectedRock().getExp());
        }));

        paint.submit("Cluster", new PaintStatistic(() -> {
            RockCluster ore = model.getRockCluster();
            return ore == null ? "Dynamic" : ore.toString() + " (" + ore.getAmount() + " spawn)";
        }));

        submit(new Banking(model), new Mining(model), new Traverse(model));
    }
}
