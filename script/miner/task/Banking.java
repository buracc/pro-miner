package script.miner.task;

import script.miner.data.ProMinerModel;
import script.api.framework.ProTask;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.predicate.NamePredicate;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;

public final class Banking extends ProTask<ProMinerModel> {

    private SceneObject bank;

    public Banking(ProMinerModel model) {
        super(model);
    }

    /**
     * Checks if the script can deposit mined ores.
     * @return true if requirements are met.
     */
    @Override
    public boolean validate() {
        return !model.isPowermining() && Inventory.isFull()
                && model.getSelectedMine().getNearBank().distance() < 50
                && (bank = SceneObjects.getNearest("Bank booth", "Bank chest")) != null;
    }

    /**
     * Continuously executes the written logic until validate returns false.
     * @return the time to let the script thread sleep (in milliseconds) after every loop.
     */
    @Override
    public int execute() {
        if (!Bank.isOpen() && bank.interact(x -> true)) {
            Time.sleepUntil(Bank::isOpen, 1200);
            return 100;
        }

        if (Bank.isOpen()) {
            Bank.depositAllExcept(new NamePredicate<>(true, "ickaxe"));
        }
        return 200;
    }
}
