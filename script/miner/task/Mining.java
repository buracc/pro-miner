package script.miner.task;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.ui.Log;
import script.miner.data.ProMinerModel;
import script.api.framework.ProTask;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Definitions;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.commons.predicate.NamePredicate;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.AnimationListener;
import org.rspeer.runetek.event.listeners.ObjectSpawnListener;
import org.rspeer.runetek.event.types.AnimationEvent;
import org.rspeer.runetek.event.types.ObjectSpawnEvent;
import org.rspeer.runetek.providers.RSObjectDefinition;

public final class Mining extends ProTask<ProMinerModel> implements ObjectSpawnListener, AnimationListener {

    private static final String[] DROP = {" ore", "uncut", "g)"};

    private static final int MINING_ANIMATION = 625;
    private static final int HERB_ANIMATION = 5249;

    private long anim = 1;

    private Position currentRock;

    public Mining(ProMinerModel model) {
        super(model);
    }

    /**
     * Returns true if rocks can be mined, i.e. if the game character can spot mineable rocks.
     * @return
     */
    @Override
    public boolean validate() {
        return !Inventory.isFull() && model.getSelectedMine().getZone().contains(Players.getLocal());
    }

    /**
     * Continuously executes the written logic until validate returns false.
     * @return the time to let the script thread sleep (in milliseconds) after every loop.
     */
    @Override
    public int execute() {
        if (Players.getLocal().getAnimation() != -1) {
            anim = System.currentTimeMillis();
        } else {
            if (System.currentTimeMillis() % anim > 5000) {
                currentRock = null;
            }
        }

        if (Dialog.canContinue()) {
            Dialog.processContinue();
        }

        if (model.isPowermining() && !Players.getLocal().isAnimating() && !Players.getLocal().isMoving()) {
            Item[] drop = Inventory.getItems(new NamePredicate<>(true, DROP));
            if (drop.length > 0) {
                for (Item item : drop) {
                    item.interact("Drop");
                    Time.sleep(Random.low(30, 50));
                }
            }
        }

        SceneObject obj = (currentRock != null) ? null : getRock();

        if (obj != null) {
            if (obj.distance() > 4) {
                if (!Movement.isDestinationSet() || Movement.getDestinationDistance() <= 5) Movement.setWalkFlag(obj);
            } else {
                if (model.isTickManipulation()
                        && Players.getLocal().getAnimation() == HERB_ANIMATION
                        && Players.getLocal().getAnimationFrame() >= 7) {
                    currentRock = null;
                }

                if (currentRock == null || isAnimationReady()) {
                    if (model.isTickManipulation() && obj.distance() > 1
                            && Players.getLocal().getAnimation() != HERB_ANIMATION) {
                        Item a = Inventory.getFirst("Guam leaf");
                        Item b = Inventory.getFirst("Swamp tar");
                        if (a == null) {
                            Item grimyGuam = Inventory.getFirst("Grimy guam leaf");
                            if (grimyGuam != null) {
                                grimyGuam.interact("Clean");
                                return 2000;
                            }
                        }

                        if (a != null && b != null) {
                            a.interact("Use");
                            b.interact("Use");
                        }
                    }

                    if (obj.interact("Mine")) {
                        currentRock = obj.getPosition();
                    }
                }
            }
        }
        return Random.low(44, 101);
    }

    private boolean isAnimationReady() {
        return (model.isTickManipulation() && Players.getLocal().getAnimation() == MINING_ANIMATION)
                || Players.getLocal().getAnimation() == -1;
    }

    private SceneObject getRock() {
        if (model.getRockCluster() == null) {
            return SceneObjects.getNearest(o -> model.getSelectedRock().test(o));
        }

        Position position = model.getRockCluster().getBase();

        double closestDistance = Integer.MAX_VALUE;
        SceneObject closest = null;

        for (int x = -2; x < 3; x++) {
            for (int y = -2; y < 3; y++) {
                SceneObject obj = SceneObjects.getFirstAt(position.translate(x, y));
                if (obj != null && model.getSelectedRock().test(obj)) {
                    if (closest == null || obj.distance() < closestDistance) {
                        closest = obj;
                    }
                }
            }
        }
        return closest;
    }

    @Override
    public void notify(ObjectSpawnEvent e) {
        if (currentRock != null && e.getPosition().equals(currentRock)) {
            RSObjectDefinition def = Definitions.getObject(e.getId());
            if (def != null && (def.getNewColors() == null || def.getNewColors()[0] != model.getSelectedRock().getColor())) {
                currentRock = null;
            }
        }
    }

    @Override
    public void notify(AnimationEvent e) {
        if (model.isTickManipulation() && e.getSource() == Players.getLocal()
                && e.getType() == AnimationEvent.TYPE_STARTED && e.getCurrent() == MINING_ANIMATION) {
            currentRock = null;
        }
    }
}
