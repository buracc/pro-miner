package script.miner.task;

import script.miner.data.Mine;
import script.miner.data.ProMinerModel;
import script.api.framework.ProTask;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;

public final class Traverse extends ProTask<ProMinerModel> {

    private boolean reverse;
    private int toggleRunAt = Random.low(10, 35);

    public Traverse(ProMinerModel model) {
        super(model);
    }

    /**
     * Returns true if the script needs to walk to the working area.
     * @return
     */
    @Override
    public boolean validate() {
        if (Inventory.isFull()) {
            reverse = false;
            return true;
        } else if (!model.getSelectedMine().getZone().contains(Players.getLocal())) {
            reverse = true;
            return true;
        }
        return false;
    }

    /**
     * Continuously executes the written logic until validate returns false.
     * @return the time to let the script thread sleep (in milliseconds) after every loop.
     */
    @Override
    public int execute() {
        if (!Movement.isRunEnabled() && Movement.getRunEnergy() >= toggleRunAt) {
            Movement.toggleRun(true);
            toggleRunAt = Random.low(10, 35);
        }
        if (!Movement.isDestinationSet() || Movement.getDestinationDistance() <= 5) {
            Mine mine = model.getSelectedMine();
            Position dest = reverse ? mine.getZone().getCenter() : mine.getNearBank();
            if (dest.distance() > 5600) {
                for (ShiftObject shift : ShiftObject.values()) {
                    SceneObject obj = SceneObjects.getNearest(x -> x.getName().equalsIgnoreCase(shift.name)
                            && x.containsAction(reverse ? shift.reverseAction : shift.action));
                    if (obj != null && obj.interact(x -> true)) {
                        System.out.println(obj.getPosition());
                        Time.sleepUntil(() -> !Players.getLocal().isMoving(), () -> Game.isLoggedIn() && Players.getLocal().isMoving(), 1200);
                        break;
                    }
                }
            } else {
                Movement.walkTo(dest);
            }
        }
        return 200;
    }

    private enum ShiftObject {

        LADDER("Ladder", "Climb-up", "Climb-down");

        private final String name;
        private final String action, reverseAction;

        ShiftObject(String name, String action, String reverseAction) {
            this.name = name;
            this.action = action;
            this.reverseAction = reverseAction;
        }
    }
}
