package script.miner.task.ui;

import script.miner.ProMiner;
import script.miner.data.ProMinerModel;
import script.miner.data.RockCluster;
import script.api.framework.ProTask;
import script.api.framework.ScriptView;
import net.miginfocom.swing.MigLayout;
import org.rspeer.runetek.api.Game;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.component.BotTitlePane;

import javax.swing.*;
import java.util.concurrent.atomic.AtomicBoolean;

public final class ProMinerUI extends ProTask<ProMinerModel> implements ScriptView<ProMinerModel> {

    private final JFrame frame;
    private final AtomicBoolean complete;

    public ProMinerUI(ProMiner script) {
        super(script.getModel());
        ScriptMeta meta = script.getMeta();

        complete = new AtomicBoolean();
        frame = new JFrame(meta.name() + " - v" + meta.version() + " by " + meta.developer());
        frame.setLayout(new MigLayout());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        BotTitlePane.decorate(frame);

        display();
    }

    @Override
    public void bind(ProMinerModel model) {

    }

    @Override
    public void load(ProMinerModel model) {

    }

    @Override
    public void display() {
        frame.setVisible(true);
        frame.setLocationRelativeTo(Game.getCanvas());
    }

    @Override
    public void dispose() {
        bind(model);
        frame.dispose();
        complete.set(true);

        for (RockCluster group : RockCluster.values()) {
            if (model.getSelectedMine() == group.getMine()
                    && model.getSelectedRock() == group.getRock()
                    && model.isTickManipulation() == group.isTickManipulation()) {
                model.setRockCluster(group);
            }
        }
    }

    @Override
    public boolean validate() {
        return !complete.get();
    }

    @Override
    public int execute() {
        return 1000;
    }
}
