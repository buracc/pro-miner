package script.miner.data;

import org.rspeer.runetek.api.movement.position.Position;

/**
 * In this class, more information about different mines are stored, such as the available rocks in the area.
 */
public enum RockCluster {

    LUMBRIDGE_COPPER(Mine.LUMBRIDGE, Rock.COPPER, 2, false, 3228, 3145),
    VARROCK_EAST_IRON(Mine.VARROCK_EAST, Rock.IRON, 2, false, 3286, 3368),
    AL_KHARID_IRON(Mine.AL_KHARID, Rock.IRON, 3, 3295, 3310),
    MINING_GUILD_F2P_IRON(Mine.MINING_GUILD_F2P, Rock.IRON, 4, true, 3033, 9739),
//    MINING_GUILD_P2P_IRON(Mine.MINING_GUILD_F2P, Rock.IRON, 4, 3021, 9721),
    MINING_GUILD_P2P_IRON(Mine.MINING_GUILD_F2P, Rock.IRON, 4, 3029, 9720),
    ARDOUGNE_EAST_TICK_IRON(Mine.ARDOUGNE_EAST, Rock.IRON, 4, true, 2714, 3331),
    ARDOUGNE_EAST_IRON(Mine.ARDOUGNE_EAST, Rock.IRON, 3, 2692, 3329),
    QUARRY_GRANITE(Mine.QUARRY, Rock.GRANITE, 4, true, 3167, 2909),
    YANILLE_IRON(Mine.YANILLE, Rock.IRON, 3, false, 2627, 3141),
    ;

    private final Mine mine;
    private final Rock rock;
    private final int amount;
    private final boolean tickManipulation;
    private final Position base;

    RockCluster(Mine mine, Rock rock, int amount, boolean tickManipulation, int baseX, int baseY) {
        this.mine = mine;
        this.rock = rock;
        this.amount = amount;
        this.tickManipulation = tickManipulation;
        this.base = new Position(baseX, baseY);
    }

    RockCluster(Mine mine, Rock rock, int amount, int baseX, int baseY) {
        this(mine, rock, amount, false, baseX, baseY);
    }

    public Mine getMine() {
        return mine;
    }

    public Rock getRock() {
        return rock;
    }

    public int getAmount() {
        return amount;
    }

    public Position getBase() {
        return base;
    }

    public boolean isTickManipulation() {
        return tickManipulation;
    }

    @Override
    public String toString() {
        String name = super.toString();
        return name.charAt(0) + name.substring(1).toLowerCase().replace("_", " ");
    }
}
