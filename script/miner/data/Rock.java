package script.miner.data;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.providers.RSObjectDefinition;

import java.util.function.Predicate;

/**
 * Different types of rocks that are available in the game.
 */
public enum Rock implements Predicate<SceneObject> {

    CLAY(6705, 5),
    TIN(53, 17.5),
    COPPER(4645, 17.5),
    IRON(2576, 35),
    SILVER(74, 40),
    COAL(10508, 50),
    GOLD(8885, 65),
    MITHRIL(22239, 80),
    ADAMANTITE(21662, 95),
    GRANITE(5679, 61.67),
    EMPTY(-1, -1);

    private final int color;
    private final double exp;

    Rock(int color, double exp) {
        this.color = color;
        this.exp = exp;
    }

    /**
     * @return the value of the rock's color in game.
     */
    public int getColor() {
        return color;
    }

    /**
     * Validates the nearest available, mineable rock.
     * @param o the rock game object.
     * @return the validated rock game object.
     */
    public static Rock identify(SceneObject o) {
        if (o == null) {
            return null;
        }
        RSObjectDefinition definition = o.getDefinition();
        if (definition == null || !o.getName().equals("Rocks")) {
            return null;
        }

        short[] colors = definition.getNewColors();
        if (colors == null) {
            return Rock.EMPTY;
        } else if (colors.length != 1) {
            return null;
        }

        for (Rock rock : Rock.values()) {
            if (colors[0] == rock.color) {
                return rock;
            }
        }

        return null;
    }

    @Override
    public boolean test(SceneObject o) {
        Rock rock = identify(o);
        return rock == this;
    }

    /**
     * @return the experience gained per successful mining attempt.
     */
    public double getExp() {
        return exp;
    }
}
