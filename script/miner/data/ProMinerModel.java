package script.miner.data;

import org.rspeer.runetek.api.component.tab.Inventory;

/**
 * The script settings object. In this class, the user-defined settings (in the GUI) are saved.
 */
public final class ProMinerModel {

    private RockCluster rockCluster = RockCluster.ARDOUGNE_EAST_TICK_IRON;
    private Mine selectedMine = rockCluster.getMine();
    private Rock selectedRock = rockCluster.getRock();

    private boolean powermining = true;
    private boolean tickManipulation = rockCluster.isTickManipulation();

    /**
     * @return the selected working area of the script.
     */
    public Mine getSelectedMine() {
        return selectedMine;
    }

    /**
     * Sets the working area of the script.
     * @param selectedMine the Mine to be used.
     */
    public void setSelectedMine(Mine selectedMine) {
        this.selectedMine = selectedMine;
    }

    /**
     * @return the selected type of rock to mine.
     */
    public Rock getSelectedRock() {
        return selectedRock;
    }

    /**
     * Sets the selected rock to be mined.
     * @param selectedRock the Rock to be mined.
     */
    public void setSelectedRock(Rock selectedRock) {
        this.selectedRock = selectedRock;
    }

    /**
     * @return the rock cluster to be used along with the working area.
     */
    public RockCluster getRockCluster() {
        return rockCluster;
    }

    /**
     * Sets the rock cluster to be used along with the working area.
     * @param rockCluster the selected cluster.
     */
    public void setRockCluster(RockCluster rockCluster) {
        this.rockCluster = rockCluster;
    }

    /**
     * @return true if power mining is enabled.
     */
    public boolean isPowermining() {
        return powermining;
    }

    /**
     * Sets the power mining functionality.
     * Power mining means, mining very quickly, and instead of depositing the ores, diposing of them.
     * Players do this to train their mining skill more efficiently.
     * @param powermining true if enabled.
     */
    public void setPowermining(boolean powermining) {
        this.powermining = powermining;
    }

    /**
     * @return true if tick manipulation is enabled.
     */
    public boolean isTickManipulation() {
        //TODO support other items like herb tar
        return tickManipulation && Inventory.containsAll("Guam leaf", "Swamp tar");
    }

    /**
     * Sets the tick manipulation functionality.
     * Tick manipulation means, that the script will not wait for the game to process game animations. Game animations
     * take longer game ticks than the actual mining process. With tick manipulation, the script will skip the animations
     * and mine at a slightly faster (about 33% faster) rate.
     * @param tickManipulation true if enabled.
     */
    public void setTickManipulation(boolean tickManipulation) {
        this.tickManipulation = tickManipulation;
    }
}
