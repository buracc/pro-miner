package script.miner.data;

import org.rspeer.runetek.api.movement.position.Area;
import org.rspeer.runetek.api.movement.position.Position;

/**
 * Different types of mining locations inside the game.
 */
public enum Mine {

    LUMBRIDGE(Area.rectangular(
            new Position(3219, 3152),
            new Position(3233, 3142)
    ), new Position(3208, 3217, 2)),

    AL_KHARID(Area.rectangular(
            new Position(3287, 3273),
            new Position(3311, 3320)
    ), new Position(3278, 3176)),

    VARROCK_WEST(Area.rectangular(
            new Position(3170, 3362),
            new Position(3185, 3380)
    ), new Position(3185, 3436)),

    VARROCK_EAST(Area.rectangular(
            new Position(3277, 3358),
            new Position(3293, 3371)
    ), new Position(3253, 3420)),

    MINING_GUILD_F2P(Area.rectangular(
            new Position(3044, 9730),
            new Position(3015, 9743)
    ), new Position(3012, 3355)),

    MINING_GUILD_P2P(Area.rectangular(
            new Position(3028, 9724),
            new Position(3011, 9715)
    ), new Position(3013, 9718)),

//    MINING_GUILD_P2P(Area.rectangular(
//            new Position(3027, 9722),
//            new Position(3032, 9718)
//    ), new Position(3013, 9718)),

    ARDOUGNE_EAST(Area.rectangular(
            new Position(2718, 3337),
            new Position(2689, 3326)
    ), new Position(2655, 3283)),

    YANILLE(Area.rectangular(
            new Position(2622, 3152),
            new Position(2642, 3129 )
    ), new Position(2610, 3094)),

    QUARRY(Area.rectangular(
            new Position(3165, 2907),
            new Position(3168, 2911 )
    ), new Position(0, 0)),

    ;

    private final Area zone;
    private final Position nearBank;

    Mine(Area zone, Position nearBank) {
        this.zone = zone;
        this.nearBank = nearBank;
    }

    /**
     * Gets the working area of the script.
     * @return the working area.
     */
    public Area getZone() {
        return zone;
    }

    /**
     * Gets the nearest bank deposit box.
     * @return the position of the bank deposit box.
     */
    public Position getNearBank() {
        return nearBank;
    }
}
