package script.api.framework;

import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;

import java.awt.*;
import java.util.LinkedHashMap;
import java.util.Map;

public final class ScriptPaint implements RenderListener {

    private static final int BASE_X = 306;
    private static final int BASE_Y = 6;

    private static final int DEFAULT_WIDTH_INCR = 20;

    private static final int BASE_HEIGHT = 20;
    private static final int LINE_HEIGHT = 20;

    private static final Color FOREGROUND = Color.WHITE;
    private static final Color BACKGROUND = Color.BLACK;
    private static final Stroke STROKE = new BasicStroke(1.8f);

    private final ProScript ctx;
    private final Map<String, PaintStatistic> stats;

    private Color outline;

    /**
     * Constructs a new Paint object. A paint is a graphic that is rendered over the game frame. Users could display
     * information about the running script, such as how long the script has been running for, or what the current
     * status is of the script.
     * @param ctx the ProScript instance.
     */
    public ScriptPaint(ProScript ctx) {
        this.ctx = ctx;
        stats = new LinkedHashMap<>();
        outline = new Color(240, 0, 0);

        ScriptMeta meta = ctx.getMeta();
        stats.put(meta.name(), new PaintStatistic(true, () -> "v" + meta.version() + " by " + meta.developer()));
        stats.put("Runtime", new PaintStatistic(ctx.runtime::toElapsedString));
    }

    /**
     * Gets the outline color of the paint.
     * @return the color.
     */
    public Color getOutline() {
        return outline;
    }

    /**
     * Sets the outline color of the paint.
     * @param outline the color to be used.
     */
    public void setOutline(Color outline) {
        this.outline = outline;
    }

    /**
     * Submits a new statistic tracker for the paint to be displayed/drawn.
     * @param key the title of the tracked data.
     * @param tracker the tracked data.
     */
    public void submit(String key, PaintStatistic tracker) {
        stats.put(key, tracker);
    }

    /**
     * Rendering hook, is invoked every time the game renders something i.e. 50 times a second at 50 FPS.
     * @param e the RenderEvent that gets notified.
     */
    @Override
    public void notify(RenderEvent e) {
        Graphics2D g = (Graphics2D) e.getSource();
        Composite defaultComposite = g.getComposite();

        int width = 180;
        int currentX = BASE_X + (DEFAULT_WIDTH_INCR / 2);
        int currentY = BASE_Y + (LINE_HEIGHT / 2);

        g.setStroke(STROKE);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(FOREGROUND);

        for (Map.Entry<String, PaintStatistic> entry : stats.entrySet()) {
            PaintStatistic stat = entry.getValue();
            String string = entry.getKey() + (stat.isHeading() ? " - " : ": ") + stat.toString();
            int currentWidth = g.getFontMetrics().stringWidth(string);
            if (currentWidth > width) {
                width = currentWidth;
            }
        }

        g.setComposite(AlphaComposite.SrcOver.derive(0.5f));
        g.setColor(BACKGROUND);
        g.fillRoundRect(BASE_X, BASE_Y, width + DEFAULT_WIDTH_INCR, (stats.size() * LINE_HEIGHT) + BASE_HEIGHT, 7, 7);

        g.setComposite(defaultComposite);
        g.setColor(outline);
        g.drawRoundRect(BASE_X, BASE_Y, width + DEFAULT_WIDTH_INCR, (stats.size() * LINE_HEIGHT) + BASE_HEIGHT, 7, 7);

        g.setColor(FOREGROUND);
        for (Map.Entry<String, PaintStatistic> entry : stats.entrySet()) {
            PaintStatistic stat = entry.getValue();

            String string = entry.getKey() + (stat.isHeading() ? " - " : ": ") + stat.toString();
            int drawX = currentX;
            if (stat.isHeading()) {
                drawX = BASE_X + ((width + DEFAULT_WIDTH_INCR) - g.getFontMetrics().stringWidth(string)) / 2;
                g.setColor(outline);
                g.drawRect(BASE_X, currentY + (LINE_HEIGHT / 2) - BASE_Y + 1, width + DEFAULT_WIDTH_INCR, LINE_HEIGHT);

                g.setComposite(AlphaComposite.SrcOver.derive(0.1f));
                g.fillRect(BASE_X, currentY + (LINE_HEIGHT / 2) - BASE_Y + 1, width + DEFAULT_WIDTH_INCR, LINE_HEIGHT);
                g.setComposite(defaultComposite);

                g.setFont(g.getFont().deriveFont(Font.BOLD));
            } else {
                g.setFont(g.getFont().deriveFont(Font.PLAIN));
            }

            g.setColor(FOREGROUND);
            g.drawString(string, drawX, currentY += LINE_HEIGHT);
        }
    }
}
