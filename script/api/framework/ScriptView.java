package script.api.framework;

public interface ScriptView<Model> {

    /**
     * Bind data from the settings GUI, to the script instance.
     * @param model the settings instance.
     */
    void bind(Model model);

    /**
     * Loads saved settings into the GUI.
     * @param model the settings instance.
     */
    void load(Model model);

    /**
     * Display the settings GUI window.
     */
    void display();

    /**
     * Disposes of the GUI.
     */
    void dispose();
}
