package script.api.framework;

import org.rspeer.runetek.api.commons.StopWatch;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.task.TaskScript;

public abstract class ProScript<Model> extends TaskScript implements RenderListener {

    protected final Model model;
    protected final ScriptPaint paint;
    protected final StopWatch runtime;

    /**
     * Constructs a new script instance.
     * @param model the class that handles script settings.
     */
    public ProScript(Model model) {
        this.model = model;
        runtime = StopWatch.start();
        paint = new ScriptPaint(this);
    }

    /**
     * @return Retrieves the script setting object.
     */
    public Model getModel() {
        return model;
    }

    /**
     * Rendering hook, gets called each time the game renders something i.e. 50 times a second at 50 FPS.
     * @param e the RenderEvent that gets notified.
     */
    @Override
    public void notify(RenderEvent e) {
        paint.notify(e);
    }
}
