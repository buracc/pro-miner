package script.api.framework;

import script.api.commons.NumericFormat;
import org.rspeer.runetek.api.commons.StopWatch;

import java.util.function.IntSupplier;
import java.util.function.Supplier;

public final class PaintStatistic {

    private final boolean heading;
    private final Supplier<String> supplier;

    /**
     * Constructs a statistic object with a String supplier.
     * @param heading true if text will be displayed as a heading, for example a title.
     * @param supplier the value that should be displayed.
     */
    public PaintStatistic(boolean heading, Supplier<String> supplier) {
        this.heading = heading;
        this.supplier = supplier;
    }

    /**
     * Constructs a statistic object with a String supplier.
     * @param supplier the value that should be displayed.
     */
    public PaintStatistic(Supplier<String> supplier) {
        this(false, supplier);
    }

    /**
     * Constructs a statistic object with a time tracker, and an int value.
     * Example: To track the amount of rocks mined, including hourly mine rate.
     * @param runtime the stopwatch object.
     * @param rate the value.
     */
    public PaintStatistic(StopWatch runtime, IntSupplier rate) {
        this(false, () -> {
            int value = rate.getAsInt();
            return NumericFormat.apply(value) + " (" + NumericFormat.apply((long) runtime.getHourlyRate(value)) + " / hr)";
        });
    }

    /**
     * @return Returns true if a statistic is a heading.
     */
    public boolean isHeading() {
        return heading;
    }

    /**
     * @return The String value of the statistic.
     */
    public String toString() {
        return supplier.get();
    }
}