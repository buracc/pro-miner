package script.api.framework;

import org.rspeer.script.task.Task;

public abstract class ProTask<Model> extends Task {

    protected final Model model;

    /**
     * Constructs a new script task.
     * Example:
     * When a script is split up into 3 different tasks (Walking to the location, mining rocks, depositing the
     * rocks into your storage), you could have 3 ProTasks that handle automation of these tasks: Mining, Walking, Depositing.
     * @param model
     */
    public ProTask(Model model) {
        this.model = model;
    }
}
