Pro-Miner
--

Pro-Miner is a script/program that automates ore mining aka the Mining skill in RuneScape. 

----

to mine (/mʌɪn/)

_verb_
1. obtain (coal or other minerals) from a mine.
`"the company came to the area to mine phosphate"`
2. dig in (the earth) for coal or other minerals.
   `"the hills were mined for copper oxide"`

----

The project consists of:

- The automation core
- The commons API
- The scripting framework/API

### Automation core (miner):
This package contains the entire logic that handles all the automation/interaction done with the game. The automation core
is split up between several tasks, such as Mining, Banking and Traversing. These are tasks that require validation before
execution. For instance, the game character should only execute the Mining task if the character is already at the location
which contains the rocks to be mined. Else, it should execute the Traversing task to traverse to the location. 

### Commons API:
This package contains useful utility classes and methods which are for example used to draw graphics on top of the game 
screen, or manipulate data values.

### Scripting framework
This package consists of multiple classes that make up the scripting framework. The scripting framework makes it easier
for the developer to 'script' simple tasks that should be automated within the game. The genericity of the framework
allows it to be customly typed. This may come in handy when the developer decides to, for example, allow the user of the
program to adjust/configure the settings.